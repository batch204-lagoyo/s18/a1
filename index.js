console.log("Hello World");

function getSum(num1, num2) {
  console.log("Displayed the sum of " + num1 + " and " + num2);
  console.log(num1 + num2);
}
getSum(5, 15);

function getDiff(numo1, numo2) {
  console.log("Displayed the difference of " + numo1 + " and " + numo2);
  console.log(numo1 - numo2);
}

getDiff(20, 5);

function returnProduct(num1, num2) {
  let product = num1 * num2;
  return product;
}
let myProduct = returnProduct(50, 10);
console.log("The Product of 50 and 10");
console.log(myProduct);
// Quotient
function returnQuotient(num1, num2) {
  let quotient = num1 / num2;
  return quotient;
}
let myQuotient = returnProduct(50, 10);
console.log("The Quotient of 50 and 10");
console.log(myQuotient);

function areaCircle(pi, radius) {
  let area = pi * radius ** 2;
  return area;
}
let areaCircles = areaCircle(3.1416, 15);
console.log("The result of getting the area of circle with 15 radius:");
console.log(areaCircles);
// average
function getAve(numOne, numTwo, numThree, numFour) {
  console.log("The average of getting the sum of 20,40,60,80:");
  console.log(numOne + numTwo + numThree + numFour / 4);
}
getAve(20, 40, 60, 80);

function isPassingScore(score, total_score) {
  let percentage = (score / total_score) * 100;
  let isPassed = percentage >= 75;
  return isPassed;
}
let myScore = isPassingScore(38, 50);
console.log("Is 38/50 passing score?:");
console.log(myScore);
